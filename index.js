
//Requisitos
const Discord = require("discord.js");
const config = require("./bot.json");



//Variaveis
let token = (process.env.TOKEN)

//Client Open
const client = new Discord.Client();

//Menção
client.on("message", message => {
if(message.author.bot) return;
if(message.content == `<@!${client.user.id}>` || message.content == `<@${client.user.id}>`) return message.channel.send(`Olá, meu nome e astralbix mais pode me chama de astra sou um bot cheio de funçoes, meu prefixo e !.`)
});

let status = ('idle')
//Status
client.on("ready", () => {
  console.log('BOT ON !!')
  let activities = [
      `Estou em ${client.guilds.cache.size} servidores !`,
    ],
    i = 0;
  setInterval( () => client.user.setActivity(`${activities[i++ % activities.length]}`, {
        type: "WATCHING"
      }), 1000 * 60); 
  client.user
      .setStatus(status)
});

//Handler
client.on('message', message => {
  if (message.author.bot) return;
  if (!message.content.toLowerCase().startsWith(config.prefix.toLowerCase())) return;
  if (message.content.startsWith(`<@!${client.user.id}>`) ||
  message.content.startsWith(`<@${client.user.id}>`)) return;


//Pastes
 const args = message.content
    .trim().slice(config.prefix.length)
    .split(/ +/g);
  const command = args.shift().toLowerCase();
  try {
    const commandFile = require(`./commands/${command}.js`)
    commandFile.run(client, message, args);
   } catch (err) {
     message.channel.send(`Este comando não existe ou talvez foi escrito errado...`)
  }
});


//ligar
client.login(token)
